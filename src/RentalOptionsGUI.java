import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JFrame;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.table.DefaultTableModel;

public class RentalOptionsGUI {
	private JComboBox<String> genreDropdown;
	private String[] genreList;
	private JLabel genreLabel;
	
	private JButton returnButton;
	private JButton rentButton;
	private JButton quitButton;
	
	private JTable arrayValsTable;
	private DefaultTableModel myTableModel;
	private String[][] tableVals;
	private String[] columnRentHeadings = {"Movie ID", "Movie Title", 
										"Genre", "Price"};
	private String[] columnReturnHeadings = {"Rental ID", "Movie Title", 
										"Genre", "Checkout Date"};
	
	private JFrame returnFrame;
	private JFrame rentFrame;
	private ArrayList<Movie> movieList;
	private String IDInput;
	
	public void checkOutMovie(String memberID) {
		rentFrame = new JFrame("Check Out Menu");
		rentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		rentFrame.setSize(1000, 1000);							//Set size of the rent frame
		rentFrame.setLocationRelativeTo(null);					//Center the frame
		rentFrame.setVisible(true);								//Display the frame
		
		//Create table for movie display
		myTableModel = new DefaultTableModel(columnRentHeadings, 0)
		{
			
			@Override
			public boolean isCellEditable(int row, int column) {
				//all cells false
				return false;
			}
		};
		arrayValsTable = new JTable(myTableModel);
		
		
		rentButton = new JButton("Rent");		//Create the rent movie button
	    rentButton.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event) {
						//Get a valid movie ID
						int[] indexInput = arrayValsTable.getSelectedRows();
						
						if (indexInput.length < 1) {
							JOptionPane.showMessageDialog(null, "You have not selected a movie.");
						}	//End if
						else {
							//Get list of payment methods and validate input
				        	ArrayList<String> paymentMethodsList = new ArrayList<String>();
							paymentMethodsList = MovieDatabase.getPaymentMethodList();
							String[] payArray = paymentMethodsList.toArray(new String[0]);
							String payIn = JOptionPane.showInputDialog(null, "Please choose a payment method:", "Payment Method",
									JOptionPane.QUESTION_MESSAGE, null, payArray, payArray[0]).toString(); 
							
							for (int arrayIndex = 0; arrayIndex < indexInput.length; ++arrayIndex) {
								IDInput = myTableModel.getValueAt(indexInput[arrayIndex], 0).toString();
								MovieDatabase.insertIntoRental(memberID, IDInput, payIn);
								MovieDatabase.decreaseMovieQuantity(IDInput);
							}	//End for
							//Message depends on the amount of movies the user has rented
							String rentFinishMessage = "You have rented " + indexInput.length;
							if (indexInput.length > 1) {
								JOptionPane.showMessageDialog(null, rentFinishMessage + " movies.");
							}
							else {
								JOptionPane.showMessageDialog(null, rentFinishMessage + " movie.");
							}
							
							movieList.clear();
							arrayValsTable.removeAll();
							
							updateRentTable(genreDropdown.getSelectedItem().toString());
						}	//End else
					}	//End method actionPerformed
				}	//End ActionListener()
		);
	    
	    quitButton = new JButton("Quit");			//Create the quit button
	    quitButton.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event) {
						rentFrame.dispose();
					}	//End method actionPerformed
				}	//End ActionListener()
		);
	    
		//Movies by category
		movieList = new ArrayList<Movie>();
		
		//Get list of genres and validate input
		ArrayList<String> availableGenreList = new ArrayList<String>();
		availableGenreList = MovieDatabase.getGenres();
		genreList = new String[availableGenreList.size() + 1];
		genreList[0] = "All";
		for (int genreIndex = 0; genreIndex < availableGenreList.size(); ++genreIndex) {
			genreList[genreIndex + 1] = availableGenreList.get(genreIndex);
		}	//End for loop which adds the genres to the list
		genreLabel = new JLabel("Select a genre:");
		genreDropdown = new JComboBox<String>(genreList);
		
		rentTableLayout();		//Formats the GUI table
		updateRentTable("All");
		
		genreDropdown.addItemListener(
			new ItemListener()
			{
				public void itemStateChanged(ItemEvent event)
				{
					if(event.getStateChange() == ItemEvent.SELECTED) {
						String genreInput = genreDropdown.getSelectedItem().toString();
						arrayValsTable.removeAll();
						movieList.clear();

						updateRentTable(genreInput);
					}
				}	//End method itemStateChanged
			}	//End anonymous inner class
		);	//End call to addItemListener
				
		
	}	//End checkOutMovie
	
	
	
	//Return a movie that is of the inputted movie genre
	public static ArrayList<Movie> getMoviesByCat(String movieCat) {
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		
		movieList = MovieDatabase.getMoviesInCat(movieCat);
		
		return movieList;
		
	}	//End displayMoviesByGenre
	
	
	//Creates the table layout
		public void rentTableLayout() {
			GridBagConstraints layoutConst = null;
			rentFrame.setTitle("Movies Available to Rent");
			
			//Position the table
			rentFrame.setLayout(new GridBagLayout());
			layoutConst = new GridBagConstraints();
		    layoutConst.insets = new Insets(10, 10, 1, 0);
		    layoutConst.fill = GridBagConstraints.HORIZONTAL;
		    layoutConst.gridx = 0;
		    layoutConst.gridy = 0;
		    rentFrame.add(genreLabel, layoutConst);
		    
		    layoutConst = new GridBagConstraints();
		    layoutConst.insets = new Insets(1, 10, 10, 0);
		    layoutConst.fill = GridBagConstraints.HORIZONTAL;
		    layoutConst.gridx = 0;
		    layoutConst.gridy = 1;
		    rentFrame.add(genreDropdown, layoutConst);
		 
		    //Position the column headers
			layoutConst = new GridBagConstraints();
		    layoutConst.insets = new Insets(1, 10, 0, 0);
		    layoutConst.fill = GridBagConstraints.HORIZONTAL;
		    layoutConst.gridx = 0;
		    layoutConst.gridy = 2;
		    layoutConst.gridwidth = 4;
		    rentFrame.add(arrayValsTable.getTableHeader(), layoutConst);
		    
			layoutConst = new GridBagConstraints();
			layoutConst.insets = new Insets(1,10, 0, 0);
			layoutConst.fill = GridBagConstraints.HORIZONTAL;
			layoutConst.gridx = 0;
			layoutConst.gridy = 3;
			layoutConst.gridwidth = 4;
			rentFrame.add(arrayValsTable, layoutConst);
			
		    
		    layoutConst = new GridBagConstraints();
		    layoutConst.insets = new Insets(0, 10, 10, 5);
		    layoutConst.fill = GridBagConstraints.HORIZONTAL;
		    layoutConst.gridx = 5;
		    layoutConst.gridy = 5;
		    rentFrame.add(rentButton, layoutConst);

		    layoutConst = new GridBagConstraints();
		    layoutConst.insets = new Insets(0, 5, 10, 10);
		    layoutConst.fill = GridBagConstraints.HORIZONTAL;
		    layoutConst.gridx = 6;
		    layoutConst.gridy = 5;
		    rentFrame.add(quitButton, layoutConst);
			
		}	//End tableLayout
		
		
		//Fill the table with the information from the arraylists
		public void fillRentTable(ArrayList<Movie> movieList) {
			final int movieIDCol = 0;
			final int movieTitleCol = 1;
			final int movieGenreCol = 2;
			final int movieValueCol = 3;
			
			myTableModel.setRowCount(movieList.size());
		    for (int rowIndex = 0; rowIndex < movieList.size(); rowIndex++) {
		    	
		    	myTableModel.setValueAt(movieList.get(rowIndex).getMovieID(), rowIndex, movieIDCol);
		    	myTableModel.setValueAt(movieList.get(rowIndex).getMovieTitle(), rowIndex, movieTitleCol);
		    	myTableModel.setValueAt(movieList.get(rowIndex).getGenre(), rowIndex, movieGenreCol);
		    	myTableModel.setValueAt(movieList.get(rowIndex).getMovieValue(), rowIndex, movieValueCol);
		    }//End for loop which fills tables with query results

		}	//End fillRentTable
	
		public void updateRentTable(String genreInput) {
			//Get list of movies that are in the genre
			movieList = getMoviesByCat(genreInput);
			fillRentTable(movieList);		//Fill the GUI table
		
			
		}	//End updateRentTable
		
	//-----------------------------------------------------------------------------------------------------------------------------
	public void checkInMovie(String memberID) {
		//Display movies rented by user
		movieList = new ArrayList<Movie>();

		
		returnFrame = new JFrame("Check In Menu");
		returnFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		returnFrame.setSize(1000, 1000);							//Set size of the return frame
		returnFrame.setLocationRelativeTo(null);					//Center the frame
		returnFrame.setVisible(true);								//Display the frame
		
		//Create table for movie display
		myTableModel = new DefaultTableModel(columnReturnHeadings, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				//all cells false
				return false;
			}
		};
		arrayValsTable = new JTable(myTableModel);
		
		
		returnButton = new JButton("Return");		//Create the return movie button
		returnButton.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event) {
						//Get a valid movie ID
						int[] indexInput = arrayValsTable.getSelectedRows();
						
						if (indexInput.length < 1) {
							JOptionPane.showMessageDialog(null, "You have not selected a movie.");
						}	//End if
						else {
							for (int arrayIndex = 0; arrayIndex < indexInput.length; ++arrayIndex) {
								IDInput = myTableModel.getValueAt(indexInput[arrayIndex], 0).toString();
		
								//update checkin date column of mm_rental table
								MovieDatabase.updateCheckinDate(IDInput);
								
								//update movie_qty column of mm_movie table
								MovieDatabase.increaseMovieQuantity(IDInput);
								
							}	//End for
							//Message depends on the amount of movies the user has returned
							String rentFinishMessage = "You have returned " + indexInput.length;
							if (indexInput.length > 1) {
								JOptionPane.showMessageDialog(null, rentFinishMessage + " movies.");
							}
							else {
								JOptionPane.showMessageDialog(null, rentFinishMessage + " movie.");
							}
							
							movieList.clear();
							arrayValsTable.removeAll();
							
							updateReturnTable(memberID);
						}	//End else
					}	//End method actionPerformed
				}	//End ActionListener()
		);
	    
	    quitButton = new JButton("Quit");			//Create the quit button
	    quitButton.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event) {
						returnFrame.dispose();
					}	//End method actionPerformed
				}	//End ActionListener()
		);
	    
		returnTableLayout();
		updateReturnTable(memberID);
		
	}	//End checkInMovie
	
	
	//Fill the table with the information from the movie list
	public void fillReturnTable(ArrayList<Movie> movieList) {
		final int rentalIDCol = 0;
		final int movieTitleCol = 1;
		final int movieGenreCol = 2;
		final int checkoutDateCol = 3;
		
		myTableModel.setRowCount(movieList.size());	//Set the table model row count to the movie list size
	    for (int rowIndex = 0; rowIndex < movieList.size(); rowIndex++) {
	    	
	    	myTableModel.setValueAt(movieList.get(rowIndex).getRentalID(), rowIndex, rentalIDCol);
	    	myTableModel.setValueAt(movieList.get(rowIndex).getMovieTitle(), rowIndex, movieTitleCol);
	    	myTableModel.setValueAt(movieList.get(rowIndex).getGenre(), rowIndex, movieGenreCol);
	    	myTableModel.setValueAt(movieList.get(rowIndex).getCheckoutDate(), rowIndex, checkoutDateCol);
	    }//End for loop which fills tables with query results
	}	//End fillReturnTable
	
	public void updateReturnTable(String memberID) {
		//Get list of movies that the user has checked out
		movieList = MovieDatabase.fillListWithRentedMovies(memberID);
		if (movieList.size() < 1) {
			JOptionPane.showMessageDialog(null, "You do not have any movies to return.");
		}
		else {
			fillReturnTable(movieList);		//Fill the GUI table
		}
	}	//End updateReturnTable
	
	
	public void returnTableLayout() {
		GridBagConstraints layoutConst = null;
		returnFrame.setTitle("Movies You Have Rented");
		
		//Position the table
		returnFrame.setLayout(new GridBagLayout());
	 
	    //Position the column headers
		layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(1, 10, 0, 0);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 0;
	    layoutConst.gridy = 1;
	    layoutConst.gridwidth = 4;
	    returnFrame.add(arrayValsTable.getTableHeader(), layoutConst);
	    
		layoutConst = new GridBagConstraints();
		layoutConst.insets = new Insets(1,10, 0, 0);
		layoutConst.fill = GridBagConstraints.HORIZONTAL;
		layoutConst.gridx = 0;
		layoutConst.gridy = 2;
		layoutConst.gridwidth = 4;
		returnFrame.add(arrayValsTable, layoutConst);
		
	    
	    layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(0, 10, 10, 5);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 5;
	    layoutConst.gridy = 4;
	    returnFrame.add(returnButton, layoutConst);

	    layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(0, 5, 10, 10);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 6;
	    layoutConst.gridy = 4;
	    returnFrame.add(quitButton, layoutConst);
		
	}	//End printMoviesToRentInfo
	
}	//End class
