import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;

public class MainScreen extends JFrame{
	private JTextArea textArea1;
	private FlowLayout layout;
	private JButton loginButton;
	private String welcomeString = "************************************************************************************\n" +
						           	"*                                                                                  *\n" +
							       	 "*                                  Movie Rental                                    *\n" +
							       	 "*                                                                                  *\n" +
							       	 "*              Created By: Brian Tran, Michael Guynn, Kyle Bakanovic               *\n" +
							       	 "*                           Lasted Edited on 04/26/2023                            *\n" +
							       	 "*                                                                                  *\n" +
							       	 "*              This program allows user to login and select movies to              *\n" +
							       	 "*              check out or return.                                                *\n" +
							       	 "*                                                                                  *\n" +
							         "************************************************************************************\n";
	
	public MainScreen() {
		//Create layout
		layout = new FlowLayout();
		setLayout(layout);
		//Create text area and add it to the layout
		textArea1 = new JTextArea(welcomeString, 10, 15);
		add(textArea1);
		//Create button and add it to the layout
		loginButton = new JButton("Login");
		add(loginButton);
		//Add action listener to the button
		
		loginButton.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event) {
						String inputMemberID = getUserID();
						RadioButtonFrame mainOptions = new RadioButtonFrame();
						mainOptions.setSize(getPreferredSize());
						mainOptions.setVisible(true);
					}	//End method actionPerformed
				}	//End ActionListener()
		);
		
		
	}	//MainScreen constructor
	
	public static String getUserID() {
		String inputMemberID = "";
		ArrayList<String> memberIDList = new ArrayList<String>();
		boolean validID = false;
		
		memberIDList = MovieDatabase.getAllMemberIDs();
		while (!validID) {
			inputMemberID = JOptionPane.showInputDialog("Enter your member ID:");
			
			for (String items : memberIDList) {
				if (inputMemberID.equals(items)) {
					return inputMemberID;
				}	//End if
			}	//End for
			
			JOptionPane.showMessageDialog(null, "This is an invalid member ID.");
		}	//End while
		return inputMemberID;
	}	//End method getUserID
	
public static String getMainOptions() {
		
		String menuInput = "none";
		boolean validInput = false;
			
		while (!validInput) {
			System.out.print("\n1. Check Out Movie" +
                "\n2. Check In Movie" +
		         "\n3. Exit");
			System.out.print("\n\nSelect Menu Option (1, 2, or 3): ");
			menuInput = scnr.nextLine();
				
			//Checks if the input is one of the available options
			if (menuInput.equals("1") || menuInput.equals("2") || menuInput.equals("3")) {
				validInput = true;
			}
			else {
				//Prints if menu input is invalid
				System.out.print("\nEnter Valid Menu Option (1, 2, or 3): ");
			}
		
		}	//End while
		
		return menuInput;
		
	}	//End getMainOptions

public class RadioButtonFrame extends JFrame {
	private JRadioButton CheckOut;
	private JRadioButton CheckIn;
	private JRadioButton Exit;
	private ButtonGroup radioGroup;
	
	public RadioButtonFrame() {
		setLayout(new FlowLayout());
		
		CheckOut = new JRadioButton("Check Out");
		CheckIn = new JRadioButton("Check In");
		Exit = new JRadioButton("Exit");
		add(CheckOut);
		add(CheckIn);
		add(Exit);
		
		radioGroup = new ButtonGroup();
		radioGroup.add(CheckOut);
		radioGroup.add(CheckIn);
		radioGroup.add(Exit);
		
	}	//Endconstructor
	
	private class RadioButtonHandler implements ItemListener
	{
		public void itemStateChanged(ItemEvent event) {
			if (CheckOut.isSelected()) {
				
			}
			else if(CheckIn.isSelected()) {
				
			}
			else if(Exit.isSelected()) {
				
			}
		}	//End method itemStateChanged()
	}	//End RadioButtonHandler class
}	//End RadioButtonFrame class
	
}	//End class
