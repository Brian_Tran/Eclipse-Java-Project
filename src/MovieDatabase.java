import java.util.ArrayList;
import java.sql.*;

public class MovieDatabase {

	public static ArrayList<String> getAllMemberIDs() {
		ArrayList<String> memberIDList = new ArrayList<String>();
		
        String sqlQuery = "SELECT MEMBER_ID " +
        						"FROM MM_MEMBER ";
	        
	    memberIDList = getResultFromSQL(sqlQuery, "member_ID");
	    
		return memberIDList;
	}	//End getAllMemberIDs
	
	public static ArrayList<String> getGenres() {
		ArrayList<String> genreList = new ArrayList<String>();
		
        String sqlQuery = "SELECT movie_category " 
        						+  "FROM MM_MOVIE_TYPE "
        						+ "ORDER BY movie_category";
        
        genreList = getResultFromSQL(sqlQuery, "movie_category");
		
		return genreList;
	}	//End getAllMemberIDs
	
	
	
	public static ArrayList<String> getResultFromSQL(String sqlQuery, String columnName) {
		ArrayList<String> resultList = new ArrayList<String>();
		
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class", "fall", "teamFall"); 
	        
	        Statement stmt = con.createStatement();
	        
	        ResultSet result = stmt.executeQuery(sqlQuery);
	        
	        while(result.next()) {
	        	resultList.add(result.getString(columnName));
	        }	//Add movie categories to the arraylist
	        
		}
	 catch (ClassNotFoundException e) {
            e.printStackTrace();
		} catch (SQLException e) {
            e.printStackTrace();
		}
		finally{
            try {
                  if(con!=null) con.close(); // close connection
            } catch (SQLException e) {
                  e.printStackTrace();
            }
		}
		
		return resultList;
	}	//End getResultFromSQL
	
	
	public static ArrayList<Movie> getMoviesInCat(String movieCat) {
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class", "fall", "teamFall"); 
	        
	        Statement stmt = con.createStatement();
	        
	        String sqlQuery = "";
	        
	        if (movieCat.equals("All")) {
	        	sqlQuery = "SELECT MOVIE_ID, MOVIE_TITLE, MOVIE_CATEGORY, MOVIE_VALUE"
						+ "\n" + "	FROM MM_MOVIE M JOIN MM_MOVIE_TYPE T "
						+ "\n" + "		ON M.MOVIE_CAT_ID = T.MOVIE_CAT_ID "
						+ "\n" + "	WHERE MOVIE_QTY > 0"
						+ "\n" + "	ORDER BY MOVIE_TITLE";
	        }	//Checks if the inputted movie cat is all
	        else {
	        	sqlQuery = "SELECT MOVIE_ID, MOVIE_TITLE, MOVIE_CATEGORY, MOVIE_VALUE"
	        						+ "\n" + "	FROM MM_MOVIE M JOIN MM_MOVIE_TYPE T "
									+ "\n" + "		ON M.MOVIE_CAT_ID = T.MOVIE_CAT_ID "
									+ "\n" + "	WHERE MOVIE_CATEGORY = " + "\'" + movieCat + "\'"
									+ "\n" + "	AND MOVIE_QTY > 0"
									+ "\n" + "	ORDER BY MOVIE_TITLE";
	        }	//Checks if the inputted movie cat is something other than all
	        
	        ResultSet result = stmt.executeQuery(sqlQuery);
	        
	        while(result.next()) {
	        	Movie object = new Movie();
	        	
	        	object.setMovieID(result.getString("movie_id"));
	        	object.setMovieTitle(result.getString("movie_title"));
	        	object.setGenre(result.getString("movie_category"));
	        	object.setMovieValue(result.getString("movie_value"));
	        	
	        	movieList.add(object);
	        }	//Add movies in the movie genre to the arraylists
	        
		}	//End try
	 catch (ClassNotFoundException e) {
            e.printStackTrace();
		} catch (SQLException e) {
            e.printStackTrace();
		}
		finally{
            try {
                  if(con!=null) con.close(); // close connection
            } catch (SQLException e) {
                  e.printStackTrace();
            }
		}	//End finally
		
		return movieList;
	}	//End getMoviesInCat
	
	
	public static void insertIntoRental(String memberID, String movieID, String paymentMethod) {
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class", "fall", "teamFall"); 
	        
	        Statement stmt = con.createStatement();
	        	
	        String sqlQuery = "INSERT INTO MM_RENTAL "
	        		+ "VALUES ((SELECT MAX(RENTAL_ID)FROM MM_RENTAL) + 1"
	        		+ ", " + memberID + ", " + movieID + ", SYSDATE, NULL,(SELECT PAYMENT_METHODS_ID "
	        		+ "\n" + "FROM MM_PAY_TYPE"
	        				+ "\n" + "WHERE PAYMENT_METHODS = " + "\'" + paymentMethod + "\'" + "))";
	        ResultSet result = stmt.executeQuery(sqlQuery);
	             
		}
	 catch (ClassNotFoundException e) {
            e.printStackTrace();
		} catch (SQLException e) {
            e.printStackTrace();
		}
		finally{
            try {
                  if(con!=null) con.close(); // close connection
            } catch (SQLException e) {
                  e.printStackTrace();
            }
		}

	}	//End rent
	
	public static ArrayList<String> getPaymentMethodList() {
		ArrayList<String> paymentMethodsList = new ArrayList<String>();
		
        String sqlQuery = "SELECT payment_methods"
        				+ "\n" + "	FROM MM_PAY_TYPE";
	        
	    paymentMethodsList = getResultFromSQL(sqlQuery, "payment_methods");
	    
		return paymentMethodsList;
	}	//getPaymentMethodList
	
	
	public static void decreaseMovieQuantity(String movieID) {
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class", "fall", "teamFall"); 
	        
	        Statement stmt = con.createStatement();
	        	
	        String sqlQuery = "UPDATE MM_MOVIE "
	        			+ "\n" + "SET MOVIE_QTY = MOVIE_QTY - 1 " 
	        			+ "\n" + "WHERE MOVIE_ID = " + movieID;
	        ResultSet result = stmt.executeQuery(sqlQuery);
		}
	 catch (ClassNotFoundException e) {
            e.printStackTrace();
		} catch (SQLException e) {
            e.printStackTrace();
		}
		finally{
            try {
                  if(con!=null) con.close(); // close connection
            } catch (SQLException e) {
                  e.printStackTrace();
            }
		}

	}	//End decreaseMovieQuantity
	
	
	public static ArrayList<Movie> fillListWithRentedMovies(String memberID) {
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class", "fall", "teamFall"); 
	        
	        Statement stmt = con.createStatement();
	        
	        String sqlQuery = "";
	        
	        sqlQuery = "SELECT rental_id, movie_title, movie_category, checkout_date"		//SQL statement for retrieving a list 
					+ "\n" + "    FROM mm_rental JOIN mm_movie USING (movie_id)"					//of movies rented out by the user
					+ "\n" + "        JOIN mm_movie_type USING (movie_cat_id)"
					+ "\n" + "    WHERE checkin_date IS NULL AND member_id = " + memberID;
	        
	        ResultSet result = stmt.executeQuery(sqlQuery);
	        
	        while(result.next()) {
	        	Movie object = new Movie();
	        	
	        	object.setRentalID(result.getString("rental_id"));
	        	object.setMovieTitle(result.getString("movie_title"));
	        	object.setGenre(result.getString("movie_category"));
	        	object.setCheckoutDate(result.getString("checkout_date"));
	        	
	        	movieList.add(object);
	        }	//Add movies in the movie genre to the arraylists
	        
		}	//End try
	 catch (ClassNotFoundException e) {
            e.printStackTrace();
		} catch (SQLException e) {
            e.printStackTrace();
		}
		finally{
            try {
                  if(con!=null) con.close(); // close connection
            } catch (SQLException e) {
                  e.printStackTrace();
            }
		}	//End finally

		return movieList;
	}	//End fillListWithRentedMovies
	
	
	//Add 1 to the movie
	public static void increaseMovieQuantity(String rentalID) {
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class", "fall", "teamFall"); 
	        
	        Statement stmt = con.createStatement();
	        	
	        String sqlQuery = "UPDATE MM_MOVIE "
	        			+ "\n" + "SET MOVIE_QTY = MOVIE_QTY + 1 " 
	        			+ "\n" + "WHERE MOVIE_ID = " + "(SELECT movie_ID "
	        										+ "\n" + "FROM mm_rental"
	        										+ "\n" + "WHERE rental_ID = " + rentalID + ")";
	        ResultSet result = stmt.executeQuery(sqlQuery);
		}
	 catch (ClassNotFoundException e) {
            e.printStackTrace();
		} catch (SQLException e) {
            e.printStackTrace();
		}
		finally{
            try {
                  if(con!=null) con.close(); // close connection
            } catch (SQLException e) {
                  e.printStackTrace();
            }
		}

	}	//End increaseMovieQuantity
	
	//Update the checkin date when the user returns a movie
	public static void updateCheckinDate(String rentalID) {
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class", "fall", "teamFall"); 
	        
	        Statement stmt = con.createStatement();
	        	
	        String sqlQuery = "UPDATE mm_rental "
	        			+ "\n" + "SET checkin_date = SYSDATE" 
	        			+ "\n" + "WHERE rental_ID = " + rentalID;
	        ResultSet result = stmt.executeQuery(sqlQuery);
		}
	 catch (ClassNotFoundException e) {
            e.printStackTrace();
		} catch (SQLException e) {
            e.printStackTrace();
		}
		finally{
            try {
                  if(con!=null) con.close(); // close connection
            } catch (SQLException e) {
                  e.printStackTrace();
            }
		}

	}	//End updateCheckinDate


}	//End class
