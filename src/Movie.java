

//This class stores the movie record information
public class Movie {
	private String rentalID;
	private String movieID;
	private String movieTitle;
	private String genre;
	private String movieValue;
	private String checkoutDate;
	
	public String getRentalID() {
		return rentalID;
	}	//End getRentalID
	
	public void setRentalID(String userInput) {
		rentalID = userInput;
	}	//End setRentalID
	
	public String getMovieID() {
		return movieID;
	}	//End getMovieID
	
	public void setMovieID(String userInput) {
		movieID = userInput;
	}	//End setMovieID
	
	public String getMovieTitle() {
		return movieTitle;
	}	//End getMovieTitle
	
	public void setMovieTitle(String userInput) {
		movieTitle = userInput;
	}	//End setMovieTitle
	
	public String getGenre() {
		return genre;
	}	//End getGenre
	
	public void setGenre(String userInput) {
		genre = userInput;
	}	//End setGenre
	
	public String getMovieValue() {
		return movieValue;
	}	//End getMovieValue
	
	public void setMovieValue(String userInput) {
		movieValue = userInput;
	}	//End setMovieValue
	
	public String getCheckoutDate() {
		return checkoutDate;
	}	//End getCheckOutDate
	
	public void setCheckoutDate(String userInput) {
		checkoutDate = userInput;
	}	//End setCheckOutDate
	
}	//End class
