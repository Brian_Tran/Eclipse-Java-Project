import java.sql.DriverManager;
import java.sql.*;
import java.util.Scanner;

import javax.swing.JFrame;

import java.util.ArrayList;

public class MovieRentalDVD {

	public static void main(String[] args) {
		
		MainScreenGUI welcome = new MainScreenGUI();
		
	}	//End main

	
	public static void callConsole() {
		printWelcomeScreen();
		
		Scanner scnr = new Scanner(System.in);
		String memberID = getUserID(scnr);
		
		String mainMenuInput = "";
		
		do {
			mainMenuInput = getMainOptions(scnr);
			switch (mainMenuInput) {
			case "1":
				//Check out movie
				HandleMainOptions.checkOutMovie(scnr, memberID);
				break;
			case "2":
				//Check in movie
				HandleMainOptions.checkInMovie(scnr, memberID);
				break;
			case "3":
				//Exit
				System.out.print("Program exiting.  Have a good day!");
				break;
			}	//End switch
		} while (!mainMenuInput.equals("3"));
	}	//End callConsole
	
	
	public static void printWelcomeScreen() {
		System.out.print("************************************************************************************\n" +
                "*                                                                                  *\n" +
           	 "*                                  Movie Rental                                    *\n" +
           	 "*                                                                                  *\n" +
           	 "*              Created By: Brian Tran, Michael Guynn, Kyle Bakanovic               *\n" +
           	 "*                           Lasted Edited on 04/26/2023                            *\n" +
           	 "*                                                                                  *\n" +
           	 "*              This program allows user to login and select movies to              *\n" +
           	 "*              check out or return.                                                *\n" +
           	 "*                                                                                  *\n" +
		         "************************************************************************************\n");

	}	//End printWelcomeScreen
	
	
	public static String getUserID(Scanner scnr) {
		String memberID = "";
		ArrayList<String> memberIDList = new ArrayList<String>();
		boolean validID = false;
		
		memberIDList = MovieDatabase.getAllMemberIDs();
		
		
		while (!validID) {
			System.out.print("Please enter a valid memberID: ");
			memberID = scnr.nextLine();
			
			for (int listIndex = 0; listIndex < memberIDList.size(); listIndex++) {
				if (memberID.equals(memberIDList.get(listIndex))) {
					return memberID;
				}	//End if
			}	//End for
			
			System.out.println("\n" + "That is an invalid input.");
		}	//End while
		
		return memberID;
		
	}	//End getUserID
	
	
	public static String getMainOptions(Scanner scnr) {
		
		String menuInput = "none";
		boolean validInput = false;
			
		while (!validInput) {
			System.out.print("\n1. Check Out Movie" +
                "\n2. Check In Movie" +
		         "\n3. Exit");
			System.out.print("\n\nSelect Menu Option (1, 2, or 3): ");
			menuInput = scnr.nextLine();
				
			//Checks if the input is one of the available options
			if (menuInput.equals("1") || menuInput.equals("2") || menuInput.equals("3")) {
				validInput = true;
			}
			else {
				//Prints if menu input is invalid
				System.out.print("\nEnter Valid Menu Option (1, 2, or 3): ");
			}
		
		}	//End while
		
		return menuInput;
		
	}	//End getMainOptions

}	//End class
