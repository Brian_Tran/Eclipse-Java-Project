import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class CheckOutButton {
	private JLabel movieOptionsLabel;
	private JComboBox<String> movieDropdown;
	private String[] movieOptionsList;
	
	private JButton returnButton;
	private JButton quitButton;
	
	private JTable arrayValsTable;
	private String[][] tableVals;
	private String[] columnHeadings = {"Rental ID", "Movie Title", 
										"Movie Genre", "Check Out Date"};
	private static boolean isEmpty = false;		//This will tell the program if the arraylists are empty
	
	
	
	public static void main(String[] args) {	//I am using main to test the methods
		//Create a connection
		//Can remove the connection part later if necessary
		
		
		Connection movieConn = null;
			
			try { 
				
				Class.forName("oracle.jdbc.driver.OracleDriver");
				
				movieConn = DriverManager.getConnection(
						"jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class",
	                    "Fall", "teamFall");
				
				Statement stmt = movieConn.createStatement(); 				//Statement object
				
				/*
				int numMovies = getNumRentedMovies(stmt, "13");		//Testing code
				System.out.print(numMovies);
				*/
				
				//--------------------------------------------------------------------------------------
				//GUI code
				
				CheckIn userMoviesTable = new CheckIn(stmt, "1");
				
				if (!isEmpty) {
					userMoviesTable.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					userMoviesTable.pack();
					userMoviesTable.setVisible(true);
				}
				
				//--------------------------------------------------------------------------------------
				
			}	//End try
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}	//End catch
			catch (SQLException e) {
				e.printStackTrace();
			}	//End catch
			
			finally{
	            try {
	                  if(movieConn!=null) {
	                	  movieConn.close(); // close connection
	                  }	//Closes connection if it exists
	            } catch (SQLException e) {
	                  e.printStackTrace();
	            }
	        }//end finally to close connection
			
	}	//End main
	
	
	//Check In constructor
	CheckIn(Statement stmt, String memberID) {	
		
		fillArrayLists(stmt, memberID);
		int numRecords = rentalIDList.size();
		
		if (numRecords == 0) {
			 JOptionPane.showMessageDialog(null, "You do not have any checked out any movies.");
			 isEmpty = true;
		}	//Checks if the user has checked out any movies.
		else {
			returnButton = new JButton("Reserve");		//Create the return movie button
		    returnButton.addActionListener(this);
		    quitButton = new JButton("Quit");			//Create the quit button
		    quitButton.addActionListener(this);
		    
			movieOptionsLabel = new JLabel("Select a Movie to Return: (rental ID, Movie Title)");
			movieOptionsList = new String[numRecords + 1];
			movieOptionsList[0] = "Select a movie";
			for (int recordIndex = 0; recordIndex < numRecords; ++recordIndex) {
				movieOptionsList[recordIndex + 1] = "(" + rentalIDList.get(recordIndex) + ", "
												+ movieTitleList.get(recordIndex) + ")";
			}	//End for loop which adds the options to the list
			
			movieDropdown = new JComboBox<String>(movieOptionsList);		//Creates a drop down box with the movie Options
			
			tableVals = new String[numRecords][5];	//numRecords = number of rows; 5 = number of columns
			
			arrayValsTable = new JTable(tableVals, columnHeadings);
			arrayValsTable.setEnabled(false);
			
			
			fillTable();		//Fill the GUI table
			tableLayout();		//Formats the GUI table
			
		}	//End else statement which runs if the user has checked out at least one movie
		
	}	//End CheckIn constructor
	
	
	/* Called when either return or quit button is pressed. */
	   @Override
	   public void actionPerformed(ActionEvent event) {
		  String inputRentalID = "";
		  int commaIndex;
		  String movieTitle = "";
		  int recordIndex;

	      // Get source of event (2 buttons in GUI)
	      JButton sourceEvent = (JButton) event.getSource();

	      // User pressed the reserve button
	      if (sourceEvent == returnButton) {
	         inputRentalID = movieDropdown.getSelectedItem().toString();	//Get the selected string from the dropdown box
	         
	         //If the selected dropdown option is the default option
	         if (inputRentalID.equals("Select a movie")) {
	            JOptionPane.showMessageDialog(this, "Please select a movie to return.");
	         }
	         else {
		        commaIndex = inputRentalID.indexOf(",");	//Find the index of the comma
		        inputRentalID = inputRentalID.substring(1, commaIndex);		//Start at one to skip the parenthesis
		        
		        checkInMovie(inputRentalID);
		        
		        recordIndex = getMovieIndex(inputRentalID);		//Get the index of the record
		        movieTitle = movieTitleList.get(recordIndex);	
	            JOptionPane.showMessageDialog(this, movieTitle + " has been returned.");

		        removeMovieFromTableAndArrayList(recordIndex);		//Delete record from the GUI table and the arraylist
		        
	            if (rentalIDList.size() < 1) {
	            	dispose();
	            	JOptionPane.showMessageDialog(null, "All movies have been returned.");
	            }	//End if which checks if there are any more movies to return
	            
	         }	//End else
	         
	      }	//End the if statement for the return button
	      else if (sourceEvent == quitButton) {
	         dispose();                               // Terminate program
	      }	//End else if
	      
	   }	//End actionPerformed
	
	//Creates the table layout
	public void tableLayout() {
		GridBagConstraints layoutConst = null;
		setTitle("Movies You Rented");
		
		//Position the table
		setLayout(new GridBagLayout());
		layoutConst = new GridBagConstraints();
		layoutConst.insets = new Insets(1,10, 0, 0);
		layoutConst.fill = GridBagConstraints.HORIZONTAL;
		layoutConst.gridx = 0;
		layoutConst.gridy = 2;
		layoutConst.gridwidth = 4;
		add(arrayValsTable, layoutConst);
		
		//Position the column headers
		layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(1, 10, 0, 0);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 0;
	    layoutConst.gridy = 1;
	    layoutConst.gridwidth = 4;
	    add(arrayValsTable.getTableHeader(), layoutConst);
	    
	    layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(10, 10, 1, 0);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 0;
	    layoutConst.gridy = 3;
	    add(movieOptionsLabel, layoutConst);
	    
	    layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(1, 10, 10, 0);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 0;
	    layoutConst.gridy = 4;
	    add(movieDropdown, layoutConst);
	    
	    layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(0, 10, 10, 5);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 4;
	    layoutConst.gridy = 4;
	    add(returnButton, layoutConst);

	    layoutConst = new GridBagConstraints();
	    layoutConst.insets = new Insets(0, 5, 10, 10);
	    layoutConst.fill = GridBagConstraints.HORIZONTAL;
	    layoutConst.gridx = 5;
	    layoutConst.gridy = 4;
	    add(quitButton, layoutConst);
		
	}	//End tableLayout
	
	
	
	//Fill the arraylists with the information of the movies rented out by the users
	public void fillArrayLists(Statement stmt, String memberID) {
		
		//Code for getting the query results that displays the info about the movies rented out by the user
		String sqlQuery = null;
		
		sqlQuery = "SELECT rental_id, movie_id, movie_title, movie_category, checkout_date"		//SQL statement for retrieving a list 
				+ "\n" + "    FROM mm_rental JOIN mm_movie USING (movie_id)"					//of movies rented out by the user
				+ "\n" + "        JOIN mm_movie_type USING (movie_cat_id)"
				+ "\n" + "    WHERE checkin_date IS NULL AND member_id = " + memberID;
		
		try {
			ResultSet queryResults = stmt.executeQuery(sqlQuery);		//ResultSet object
			
			while(queryResults.next()) {
		    	rentalIDList.add(queryResults.getString("rental_id"));
		    	movieIDList.add(queryResults.getString("movie_id"));
		    	movieTitleList.add(queryResults.getString("movie_title"));
		    	genreList.add(queryResults.getString("movie_category"));
		    	checkOutDateList.add(queryResults.getString("checkout_date"));
		    }	//End while loop which puts the movie information for each record in array lists
		    
		}	//End try
		catch (SQLException e) {
			e.printStackTrace();
		}	//End catch
		
	}	//End fillArrayList
	
	//Fill the table with the information from the arraylists
	public void fillTable() {
		final int rentalIDCol = 0;
		final int movieIDCol = 1;
		final int movieTitleCol = 2;
		final int movieGenreCol = 3;
		final int checkOutDateCol = 4;
		
	    for (int rowIndex = 0; rowIndex < rentalIDList.size(); rowIndex++) {
	    	arrayValsTable.setValueAt(rentalIDList.get(rowIndex), rowIndex, rentalIDCol);
			arrayValsTable.setValueAt(movieIDList.get(rowIndex), rowIndex, movieIDCol);
			arrayValsTable.setValueAt(movieTitleList.get(rowIndex), rowIndex, movieTitleCol);
			arrayValsTable.setValueAt(genreList.get(rowIndex), rowIndex, movieGenreCol);
			arrayValsTable.setValueAt(checkOutDateList.get(rowIndex), rowIndex, checkOutDateCol);
	    }//End for loop which fills tables with query results

	}	//End fillTable
	
	
	//Updates the rental table to show that the movie has been checked in
	public static void checkInMovie(String rentalID) {
		Connection movieConn = null;
		
		try { 
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			movieConn = DriverManager.getConnection(
					"jdbc:oracle:thin:@oracle.gulfcoast.edu:1539:class",
                    "Fall", "teamFall");
			
			Statement stmt = movieConn.createStatement(); 
		
			String sqlStatement = null;
			sqlStatement = "UPDATE mm_rental"					//String containing the sql query
					+ "\n" + "    SET checkin_date = "
					+ "(SELECT CURRENT_DATE FROM DUAL)"
					+ "\n	WHERE rental_id = " + rentalID;
		
			ResultSet queryResults = stmt.executeQuery(sqlStatement);

		}
		catch (SQLException e) {
			e.printStackTrace();
		}	//End catch
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	//End catch
		finally{
            try {
                  if(movieConn!=null) {
                	  movieConn.close(); // close connection
                  }	//Closes connection if it exists
            } catch (SQLException e) {
                  e.printStackTrace();
            }
        }//end finally to close connection
		
	}	//End checkInMovie
	
	
	//Get the index of the movie record from the arrayList
	public static int getMovieIndex(String inputRentalID) {
		for (int listIndex = 0; listIndex < rentalIDList.size(); ++listIndex) {
	    	if (inputRentalID.equals(rentalIDList.get(listIndex))) {
	    		return listIndex;
	    	}	//Check if the inputted rental ID is the same as one of the rental IDs in the list
	    }	//Checks through each element in the rentalIDList 

		return -1; 
	}	//End getMovieIndex
	
	public void removeMovieFromTableAndArrayList(int rowIndex) {
		final int rentalIDCol = 0;
		final int movieIDCol = 1;
		final int movieTitleCol = 2;
		final int movieGenreCol = 3;
		final int checkOutDateCol = 4;
		
		//Remove the movie record from the table
		arrayValsTable.setValueAt(null, rowIndex, rentalIDCol);
		arrayValsTable.setValueAt(null, rowIndex, movieIDCol);
		arrayValsTable.setValueAt(null, rowIndex, movieTitleCol);
		arrayValsTable.setValueAt(null, rowIndex, movieGenreCol);
		arrayValsTable.setValueAt(null, rowIndex, checkOutDateCol);
		
		//Remove the movie information from the array
		rentalIDList.remove(rowIndex);
    	movieIDList.remove(rowIndex);
    	movieTitleList.remove(rowIndex);
    	genreList.remove(rowIndex);
    	checkOutDateList.remove(rowIndex);
	}	//End removeMovieFromTable
	
}	//End class
