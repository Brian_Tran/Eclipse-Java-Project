

import java.util.ArrayList;
import java.util.Scanner;

public class HandleMainOptions {
	
	public static void checkOutMovie(Scanner scnr, String memberID) {
		String checkOutMenuInput = "";
		do {
			checkOutMenuInput = getCheckOutOptions(scnr);
			String genreInput = "none";
			switch (checkOutMenuInput) {
			case "1":
				//Movies by category
				
				//Get list of genres and validate input
				ArrayList<String> availableGenreList = new ArrayList<String>();
				availableGenreList = MovieDatabase.getGenres();
				genreInput = validateGenre(scnr, availableGenreList);
				
			case "2":
				//All movies
				if (checkOutMenuInput.equals("2")) {
					genreInput = "All";
				}
				//Get list of movies that are in the genre
				ArrayList<Movie> movieList = new ArrayList<Movie>();
				movieList = getMoviesByCat(genreInput);
				printMoviesToRent(movieList);
				
				//Get a valid movie ID
				String IDInput = getValidRentInput(scnr, movieList);
				
				//Get list of payment methods and validate input
				ArrayList<String> paymentMethodsList = new ArrayList<String>();
				paymentMethodsList = MovieDatabase.getPaymentMethodList();
				String payIn = validatePaymentMethod(scnr, paymentMethodsList);
				
				MovieDatabase.insertIntoRental(memberID, IDInput, payIn);
				MovieDatabase.decreaseMovieQuantity(IDInput);
				break;
			case "3":
				//Go back
				break;
			}	//End switch
			
		} while (!checkOutMenuInput.equals("3"));
	}	//End checkOutMovie
	
	public static String validateGenre(Scanner scnr, ArrayList<String> genreList) {
		String userInput = "";
		boolean validIn = false;
		
		while (!validIn) {
			System.out.println();
			for(String item : genreList)
			{
				System.out.print(item + " | ");
			}
			
			System.out.print("\n" + "Pick a genre:");
			userInput = scnr.nextLine();
			
			for (int index = 0; index < genreList.size(); ++index) {
				if (userInput.toLowerCase().equals(genreList.get(index).toLowerCase())) {
					validIn = true;
					userInput = genreList.get(index);
				}
			}	//Goes through genre list and checks if the input is valid
			
			if (!validIn) {
				System.out.println("\n" + "That is not a valid input. Please pick one of the genres.");
			}	//If the input is invalid, output an error message
			
		}		//End while loop
		
		return userInput;
	}	//End validateGenre
	
	
	public static String validatePaymentMethod(Scanner scnr, ArrayList<String> paymentMethodsList) {
		String userInput = "";
		boolean validIn = false;
		
		while (!validIn) {
			System.out.println();
			for(String item : paymentMethodsList)
			{
				System.out.print(item + " | ");
			}
			
			System.out.print("\n" + "Pick a payment method:");
			userInput = scnr.nextLine();
			
			for (int index = 0; index < paymentMethodsList.size(); ++index) {
				if (userInput.toLowerCase().equals(paymentMethodsList.get(index).toLowerCase())) {
					validIn = true;
					userInput = paymentMethodsList.get(index);
				}
			}	//Goes through payment method list and checks if the input is valid
			
			if (!validIn) {
				System.out.println("\n" + "That is not a valid input. Please pick one of the payment methods.");
			}	//If the input is invalid, output an error message
		}//End while
		
		return userInput;
	}	//End validatePaymentMethod()
	
	//Return a movie that is of the inputted movie genre
	public static ArrayList<Movie> getMoviesByCat(String movieCat) {
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		
		movieList = MovieDatabase.getMoviesInCat(movieCat);
		
		return movieList;
		
	}	//End displayMoviesByGenre
	
	
	//Displays the movies in the list
	public static void printMoviesToRent(ArrayList<Movie> movieList) {
		if (movieList.size() < 1) {
			System.out.println("There are no movies in this category that are available to rent.");
		}	//Output error message if the movie list is empty
		else {
			System.out.printf("\n| %-5s | %-40s | %-12s | %-7s |\n", "ID", "Title", "Genre", "Price");
			System.out.print("-".repeat(77));
				
			for (Movie object : movieList) {	
				System.out.printf("\n  %-5s   %-40s   %-12s   $%-7s",
						          object.getMovieID(),
						          object.getMovieTitle(),
						          object.getGenre(),
						          object.getMovieValue());
			}	//End for loop that goes through the list and prints the info of movies available to rent
			
			System.out.println();
		}	//End else
		
	}	//End printMoviesToRentInfo
	
	public static String getValidRentInput(Scanner scnr, ArrayList<Movie> movieList) {
		String userInput = "";
		boolean validIn = false;
		
		while (!validIn) {	
			System.out.print("Please enter the movie ID of the movie you want to rent:");
			userInput = scnr.nextLine();
			for (Movie object : movieList) {
				if (userInput.equals(object.getMovieID())) {
					return userInput;
				}
			}	//End for loop
			if (!validIn) {
				System.out.println("\n" + "That is not a valid input. Please pick one of the movie options.");
			}	//If the input is invalid, output an error message
		}
		
		return userInput;
	}	//End validateRentInput
	
	public static String getValidPaymentMethod(Scanner scnr) {
		ArrayList<String> paymentMethodsList = new ArrayList<String>();
		paymentMethodsList = MovieDatabase.getPaymentMethodList();
		
		boolean validIn = false;
		String payIn = "";
		
		while (!validIn) {
			System.out.println();
			for (int index = 0; index < paymentMethodsList.size(); index++) {
				System.out.print(paymentMethodsList.get(index) + " | ");
			}
			
			System.out.print("\n\nEnter Payment Method: ");
			payIn = scnr.nextLine();
			for (String paymentMethod : paymentMethodsList) {
				if (payIn == paymentMethod) {
					return payIn;
				}
			}	//End for loop
			if (!validIn) {
				System.out.println("\n" + "That is not a valid input. Please pick one of the payment method options.");
			}	//If the input is invalid, output an error message
			
		}	//End while
		
		return payIn;
	}	//End getValidPaymentMethod
	
	
	//List the check out options and asks for user input
	public static String getCheckOutOptions(Scanner scnr) {
		
		String menuInput = "none";
		boolean validInput = false;
			
		while (!validInput) {
			System.out.print("\n1. Display Movies By Category" +
	                 "\n2. Display All Movies" +
			         "\n3. Go Back");
			System.out.print("\n\nSelect Menu Option (1, 2, or 3): ");
			menuInput = scnr.nextLine();
				
			//Checks if the input is one of the available options
			if (menuInput.equals("1") || menuInput.equals("2") || menuInput.equals("3")) {
				validInput = true;
			}
			else {
				//Prints if menu input is invalid
				System.out.print("\nEnter Valid Menu Option (1, 2, or 3): ");
			}
		}
		return menuInput;
	}	//End checkOutOptions
	
	
	//-----------------------------------------------------------------------------------------------------------------------------
	public static void checkInMovie(Scanner scnr, String memberID) {
		//Display movies rented by user
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		movieList = MovieDatabase.fillListWithRentedMovies(memberID);
		printCheckInInfo(movieList);
		
		//validate rental id input
		String inputRentalID = getValidRentID(scnr, movieList);
		
		//update checkin date column of mm_rental table
		MovieDatabase.updateCheckinDate(inputRentalID);
		
		//update movie_qty column of mm_movie table
		MovieDatabase.increaseMovieQuantity(inputRentalID);
	}	//End checkInMovie
	
	public static void printCheckInInfo(ArrayList<Movie> movieList) {
		if (movieList.size() < 1) {
			System.out.println("There are no movies in this category that are available to rent.");
		}	//Output error message if the movie list is empty
		else {
			System.out.printf("\n| %-11s | %-40s | %-12s | %-24s |\n", "Rental ID", "Title", "Genre", "Checkout Date");
			System.out.print("-".repeat(100));
				
			for (Movie object : movieList) {	
				System.out.printf("\n  %-11s   %-40s   %-12s   $%-24s",
						          object.getRentalID(),
						          object.getMovieTitle(),
						          object.getGenre(),
						          object.getCheckoutDate());
			}	//End for loop that goes through the list and prints the info of movies available to rent
			
			System.out.println();
		}	//End else
		
	}	//End printMoviesToRentInfo
	
	public static String getValidRentID(Scanner scnr, ArrayList<Movie> movieList) {
		String userInput = "";
		boolean validIn = false;
		
		while (!validIn) {	
			System.out.print("Please enter the rental ID of the movie you want to return:");
			userInput = scnr.nextLine();
			for (Movie object : movieList) {
				if (userInput.equals(object.getRentalID())) {
					return userInput;
				}
			}	//End for loop
			if (!validIn) {
				System.out.println("\n" + "That is not a valid input. Please pick one of the movie options.");
			}	//If the input is invalid, output an error message
		}
		
		return userInput;
	}	//End validateRentInput
	
	
	
}	//End class
