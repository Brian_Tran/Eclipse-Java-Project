import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JFrame;

public class MainScreenGUI {
	private String inputMemberID;
	private JFrame welcomeFrame;
	private JTextArea textArea1;
	private FlowLayout layout;
	private JButton loginButton;
	private String welcomeString = "                                                                                  \n" +
						           "                                                                                  \n" +
							       "                                  Movie Rental                                    \n" +
							       "                                                                                  \n" +
							       "              Created By: Brian Tran, Michael Guynn, Kyle Bakanovic               \n" +
							       "                           Lasted Edited on 04/26/2023                            \n" +
							       "                                                                                  \n" +
							       "              This program allows user to login and select movies to              \n" +
							       "              check out or return.                                                \n" +
							       "                                                                                  ";
	
	public MainScreenGUI() {
		//Create JFrame
		welcomeFrame = new JFrame("Welcome Screen!");
		welcomeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		//Create layout
		layout = new FlowLayout();
		welcomeFrame.setLayout(layout);
		
		//Create text area and add it to the layout
		textArea1 = new JTextArea(welcomeString);
		textArea1.setEditable(false);
		welcomeFrame.add(textArea1);
		
		//Create button and add it to the layout
		loginButton = new JButton("Login");
		welcomeFrame.add(loginButton);
		
		//Add action listener to the button
		loginButton.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event) {
						inputMemberID = getUserID();
						
						MainOptionsFrame mainOptions = new MainOptionsFrame();
						
						welcomeFrame.dispose();										//Get rid of the welcome frame
					}	//End method actionPerformed
				}	//End ActionListener()
		);
		
		//Set size of the welcome frame
		welcomeFrame.setSize(500, 200);
		
		//Center the frame
		welcomeFrame.setLocationRelativeTo(null);
		
		welcomeFrame.getContentPane().setBackground(Color.DARK_GRAY);
		
		//Display the frame
		welcomeFrame.setVisible(true);
		
	}	//MainScreen constructor
	
	public static String getUserID() {
		String inputMemberID = "";
		ArrayList<String> memberIDList = new ArrayList<String>();
		boolean validID = false;
		
		memberIDList = MovieDatabase.getAllMemberIDs();
		while (!validID) {
			inputMemberID = JOptionPane.showInputDialog("Enter your member ID:");
			for (String items : memberIDList) {
				if (inputMemberID.equals(items)) {
					return inputMemberID;
				}	//End if
			}	//End for
			
			JOptionPane.showMessageDialog(null, "This is an invalid member ID.");
		}	//End while
		return inputMemberID;
	}	//End method getUserID
	

	public class MainOptionsFrame {
		private JFrame OptionsFrame;
		private JLabel PickOptions;
		private GridBagConstraints layoutConst;
		private JButton CheckOut;
		private JButton	CheckIn;
		private JButton Exit;
		
		public MainOptionsFrame() {
			
			OptionsFrame = new JFrame("Option Selection Screen!");
			OptionsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			OptionsFrame.setSize(150, 200);								//Set size of the mainOptions frame
			OptionsFrame.setLocationRelativeTo(null);					//Center the frame
			OptionsFrame.setVisible(true);								//Display the frame
			
			//Create layout
			OptionsFrame.setLayout(new GridBagLayout());
			
			//Create pick options text area and add it to the layout
			PickOptions = new JLabel("Select a menu option:");
			
			layoutConst = new GridBagConstraints();
			layoutConst.gridx = 0;
			layoutConst.gridy = 0;
			layoutConst.insets = new Insets(10, 10, 10, 10);
			OptionsFrame.add(PickOptions, layoutConst);
			
			//------------------------------------------------------------------------------------
			
			//Create check out button and add it to the layout
			CheckOut = new JButton("Check Out");
			
			layoutConst = new GridBagConstraints();
			layoutConst.gridx = 0;
			layoutConst.gridy = 1;
			layoutConst.insets = new Insets(10, 10, 10, 10);
			OptionsFrame.add(CheckOut, layoutConst);
			
			//Add action listener to the CheckOut button
			CheckOut.addActionListener(
					new ActionListener()
					{
						public void actionPerformed(ActionEvent event) {
							RentalOptionsGUI checkOut = new RentalOptionsGUI();
							checkOut.checkOutMovie(inputMemberID);
						}	//End method actionPerformed
					}	//End ActionListener()
			);
			
			//Create check in button and add it to the layout
			CheckIn = new JButton("Check In");

			layoutConst = new GridBagConstraints();
			layoutConst.gridx = 0;
			layoutConst.gridy = 2;
			layoutConst.insets = new Insets(10, 10, 10, 10);
			OptionsFrame.add(CheckIn, layoutConst);
			
			//Add action listener to the CheckIn button
			CheckIn.addActionListener(
					new ActionListener()
					{
						public void actionPerformed(ActionEvent event) {
							RentalOptionsGUI checkIn = new RentalOptionsGUI();
							checkIn.checkInMovie(inputMemberID);
						}	//End method actionPerformed
					}	//End ActionListener()
			);
			
			//Create exit button and add it to the layout
			Exit = new JButton("Exit");

			layoutConst = new GridBagConstraints();
			layoutConst.gridx = 0;
			layoutConst.gridy = 3;
			layoutConst.insets = new Insets(10, 10, 10, 10);
			OptionsFrame.add(Exit, layoutConst);
			
			//Add action listener to the Exit button
			Exit.addActionListener(
					new ActionListener()
					{
						public void actionPerformed(ActionEvent event) {
							OptionsFrame.dispose();										//Get rid of the options frame
						}	//End method actionPerformed
					}	//End ActionListener()
			);
			
		}	//End constructor
	}	//End MainOptionsFrame class

}	//End class
