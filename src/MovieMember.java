
public class MovieMember {
	private String firstName;
	private String lastName;
	private String memberID;
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getMemberID() {
		return memberID;
	}
}
